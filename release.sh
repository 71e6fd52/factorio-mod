#!/bin/zsh

read version

git checkout -b release/$version
bundle exec rake bump $version

git checkout release/latest
git merge --ff-only release/$version
bundle exec rake release

git checkout master
git merge --no-ff release/$version
git push

git branch -d release/$version
