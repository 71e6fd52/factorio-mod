# frozen_string_literal: true

RSpec.describe Factorio::Cache do
  before :each do
    @cache = Factorio::Cache.new 'flib'
  end

  it 'has not any cache before' do
    expect(@cache.cache).to be_nil
  end

  it 'can get' do
    expect(@cache.get[:owner]).to eq 'raiguard'
  end

  it 'cache' do
    expect(@cache.cache).to be_nil
    @cache.get[:owner]
    expect(@cache.cache).to be_a Hash
  end
end
