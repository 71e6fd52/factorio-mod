# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
RSpec.describe Factorio::Mod do
  it 'has a version number' do
    expect(Factorio::Mod::VERSION).not_to be nil
  end

  it 'can raise error when the mod does not exist' do
    expect { Factorio::Mod.new('71e6').title }.to \
      raise_error Factorio::NoModError
  end

  it 'work good with space' do
    mod = Factorio::Mod.new 'Induction Charging'
    expect(mod.title).to eq 'Induction Charging'
    expect(mod.author).to eq 'Raeon'
  end

  it 'work good with no git source' do
    mod = Factorio::Mod.new 'rso-mod'
    expect(mod.git).to be_nil
    expect(mod.github).to be_nil
  end

  context 'get info' do
    before :all do
      @mod = Factorio::Mod.new 'flib'
      @mod.instance_variable_get(:@mod).get
    rescue Net::OpenTimeout
      retry
    end

    it 'name' do
      name = 'flib'
      expect(@mod.name).to eq name
      expect(@mod.id).to eq name
      expect(@mod.string_id).to eq name
    end

    it 'title' do
      title = 'Factorio Library'
      expect(@mod.title).to eq title
      expect(@mod.display_name).to eq title
    end

    it 'summary' do
      summary = 'A set of high-quality, commonly-used utilities for creating Factorio mods.'
      expect(@mod.summary).to eq summary
    end

    it 'GitHub URI' do
      expect(@mod.github.to_s).to eq \
        'https://github.com/factoriolib/flib'
    end

    it 'git URI' do
      expect(@mod.git.to_s).to eq \
        'https://github.com/factoriolib/flib.git'
    end

    it 'license' do
      expect(@mod.license).to eq 'mit'
    end

    it 'license link' do
      uri = 'https://opensource.org/licenses/MIT'
      expect(@mod.license_uri.to_s).to eq uri
      expect(@mod.license_link.to_s).to eq uri
    end

    it 'author' do
      expect(@mod.author).to eq 'raiguard'
    end

    it 'download' do
      expect(@mod.download_times).to be_an Integer
      expect(@mod.download_times).to be >= 10
    end

    it 'deprecate status' do
      expect(@mod.deprecated?).to be false
      deprecated = Factorio::Mod.new 'Bergius_Process'
      expect(deprecated.deprecated?).to be true
    end

    it 'latest download link' do
      expect(@mod.latest_download_uri).not_to be_nil
      expect(@mod.latest_download_uri).not_to include('login')
      expect(@mod.latest_download_link).to eq \
        @mod.latest_download_uri
    end

    it 'download list' do
      first_download = @mod.download_list.first
      expect(first_download.version).to eq '0.1.0'
      expect(first_download.game_version).to eq '0.18'
      uri = 'https://mods.factorio.com/download/flib/5ecac7e44d121d000cd77c76'
      expect(first_download.uri.to_s).to eq uri
      expect(first_download.file_name).to eq 'flib_0.1.0.zip'
      expect(first_download.released_at).to eq \
        DateTime.parse('2020-05-24T19:15:48.52000Z')
      expect(first_download.sha1).to eq \
        '55f7bbcfc0c0e831008b57c321db509bf3a25285'
      expect(first_download.dependencies.first).to be_a Factorio::Mod
      expect(first_download.dependencies.map(&:name)).to eq ['base >= 0.18.19']
    end

    it 'latest download' do
      expect(@mod.latest_download).not_to be_nil
      expect(@mod.latest_download.uri).not_to include('login')
    end

    it 'latest download for 0.18' do
      download = @mod.latest_download '0.18'
      expect(download.uri).not_to be_nil
      expect(download.uri).not_to include('login')
      expect(download.game_version).to eq '0.18'
    end

    it 'call args if cannot find latest download for 0.1' do
      expect(@mod.latest_download('0.1', -> { 1 })).to be 1
    end
  end

  context 'search' do
    before :all do
      @mods = Factorio::Mod.search 'LTN'
      @mod = @mods.find { |m| m.name == 'LTN-easier' }
    rescue Net::OpenTimeout
      retry
    end

    it 'have result' do
      @mods.any?
    end
  end

  context 'exist' do
    it 'exist' do
      Factorio::Mod.exist?('LTN-easier')
    end

    it 'not exist' do
      Factorio::Mod.exist?('71e6')
    end
  end
end
# rubocop:enable Metrics/BlockLength
