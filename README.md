# Factorio::Mod

[![pipeline status](https://gitlab.com/71e6fd52/factorio-mod/badges/master/pipeline.svg)](https://gitlab.com/71e6fd52/factorio-mod/commits/master)
[![coverage report](https://gitlab.com/71e6fd52/factorio-mod/badges/master/coverage.svg)](https://gitlab.com/71e6fd52/factorio-mod/commits/master)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'factorio-mod'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install factorio-mod

## Usage

See `spec` directory or https://www.rubydoc.info/gems/factorio-mod

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `bundle exec rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, follow `release.sh`.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/71e6fd52/factorio-mod.
