# frozen_string_literal: true

require 'bundler/setup'

require 'factorio/mod/version'

require 'factorio/mod/mod'
require 'factorio/mod/download'
require 'factorio/mod/exception'
require 'factorio/mod/cache'

require 'open-uri'
require 'net/http'
require 'nokogiri'
require 'addressable'
require 'json'

require 'active_support'
require 'active_support/core_ext/object/blank'
