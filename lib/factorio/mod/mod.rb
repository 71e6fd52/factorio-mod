# frozen_string_literal: true

module Factorio
  # Info interface
  #
  # example:
  #
  # ```
  # mod = Factorio::Mod.new 'LTN-easier'
  # puts "The author of #{mod.title} is #{mod.author}"
  # puts "The #{mod.title} is under #{mod.license}"
  # puts "It latest version can download at #{mod.latest_download[:uri]}"
  # puts "It latest version for factorio 0.16 can download at " \
  #   "#{mod.latest_download('0.16')[:uri]}"
  # ```
  class Mod
    # factorio Mod portal website URI
    MOD_URI = 'https://mods.factorio.com'
    API_URI = 'https://mods.factorio.com/api/mods/%s/full'

    attr_reader :name
    alias id name
    alias string_id name

    # @param name [String] name of the Mod
    def initialize(name)
      @name = name
      @mod = Cache.new name
    end

    # Get the log in token by username and password.
    #
    # @example
    #   print 'username: '
    #   user = gets.chomp
    #   print 'password: '
    #   pass = gets.chomp
    #   token = Factorio::Mod.login user, pass
    # @param username [String]
    # @param password [String]
    # @return [String] The log in token
    def self.login(username, password)
      data = Addressable::URI.form_encode(
        'username' => username,
        'password' => password,
      )
      result = Net::HTTP.post('https://auth.factorio.com/api-login', data)
                        .then(JSON.method(:parse))
      result.first
    rescue OpenURI::HTTPError
      raise result['message']
    end

    # Get mod name from mod card.
    #
    # @param card [Nokogiri::HTML]
    # @return [String] The name in
    def self.name_from_card(card)
      card.xpath('.//h2[@class="mod-card-title"]/a/@href').text \
          .split('/').last.then(&Addressable::URI.method(:unencode))
    end

    # Search mod.
    #
    # @param keyword [String] search keyword
    # @param version [String] factorio version, `any` for any version
    # @return [Array<Mod>] all mod searched on the first page with card cache
    def self.search(keyword, version: 'any')
      uri = Addressable::URI.join(
        MOD_URI, "/?query=#{Addressable::URI.encode keyword}&version=#{version}"
      )
      Nokogiri::HTML(URI.parse(uri).open).css('.mod-card').map do |card|
        Mod.new(name_from_card(card))
      end
    end

    # Get mod if exist.
    #
    # @param name [String] name of the mod
    # @return [Bool] is exist
    def self.exist?(name)
      name = Addressable::URI.encode(name)
      URI.parse(API_URI % name).open
      true
    rescue OpenURI::HTTPError
      false
    end

    # Extend relative links to absolute links
    # @param rel [String] relative link
    # @return [String] absolute link
    # @raise [NotLoginError]
    def self.extend_uri(rel)
      Addressable::URI.join(MOD_URI, rel)
    end

    # Get the title (also known as _display name_) of the Mod
    # @return [String] title
    def title
      @mod.get[:title]
    end
    alias display_name title

    # Get the summary of the Mod
    # @return [String] summary
    def summary
      @mod.get[:summary]
    end

    # Get the GitHub URI.
    # @return [Addressable::URI] GitHub URI
    def github
      @mod.get[:github_path].presence&.then do
        Addressable::URI.join('https://github.com', _1)
      end
    end

    # Get the git repo URI that can be cloned.
    # @return [Addressable::URI] git URI
    def git
      github&.to_s&.+('.git')&.then(&Addressable::URI.method(:parse))
    end

    # Get the license that the Mod use.
    # @return [String] license name
    def license
      @mod.get(:license, :name)
    end

    # Get the license URI.
    # @return [Addressable::URI] license URI
    def license_uri
      Addressable::URI.parse(@mod.get(:license, :url))
    end
    alias license_link license_uri

    # Get the author of the Mod.
    # @return [String] author
    def author
      @mod.get[:owner]
    end
    alias owner author

    # Whether Mod is deprecated
    # @return [Boolean] is deprecated
    def deprecated?
      @mod.get[:deprecated] || false
    end

    # Get the totle number of downloads of Mod.
    # @return [Integer] number of downloads
    def download_times
      @mod.get[:downloads_count]
    end
    alias downloads_count download_times

    # Get the latest _download_ of the Mod.
    # @param version [String] factorio version
    # @return [Download]
    def latest_download(version = nil, ifnone = nil)
      (version.blank? && download_list.last) ||
        download_list.reverse.find(ifnone) { _1.game_version == version.to_s }
    end

    # Get the _download_ for all of version.
    # @return [Array<Download>]
    def download_list
      @mod.get[:releases].dup.map(&Download.method(:create))
    end

    # Get latest download uri by download button in card
    # @return [Addressable::URI] download uri
    def latest_download_uri
      Mod.extend_uri(latest_download[:download_url])
    end
    alias latest_download_link latest_download_uri
  end
end
