# frozen_string_literal: true

module Factorio
  # The Exception that can't find Mod
  class NoModError < StandardError
    attr_reader :io

    def initialize(msg, io)
      @io = io
      super(msg)
    end
  end

  # The Exception that try to visit not exist page
  class NoPageError < StandardError
  end
end
