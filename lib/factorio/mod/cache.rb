# frozen_string_literal: true

module Factorio
  # Cache mod info
  class Cache
    attr_reader :cache

    # @param name [String] Mod name
    def initialize(name)
      @name = name
      @uri = Mod::API_URI % Addressable::URI.encode(@name)
      @cache = nil
    end

    # Get from cache or download
    # @param key [Symbol]
    # @return [Hash]
    def get(*key)
      @cache ||= URI.parse(@uri)
                    .open
                    .then(&Nokogiri.method(:HTML))
                    .then { JSON.parse(_1, symbolize_names: true) }
                    .then(&method(:deep_freeze))
      key.empty? ? @cache : @cache.dig(*key.map(&:to_sym))
    rescue OpenURI::HTTPError => e
      raise NoModError.new "MOD #{@name} not found", e.io
    end

    private

    def deep_freeze(sth)
      case sth
      when Hash then sth.each_value(&method(:deep_freeze))
      when Array then sth.each(&method(:deep_freeze))
      end
      sth.freeze
    end
  end
end
