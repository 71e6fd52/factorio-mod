# frozen_string_literal: true

module Factorio
  class Mod
    VERSION = '0.8.0'
  end
end
