# frozen_string_literal: true

module Factorio
  class Mod
    # store the entry of the _download_
    Download = Struct.new(:version, :factorio_version, :download_url,
                          :file_name, :released_at, :dependencies, :sha1) do
      alias_method :game_version, :factorio_version
      alias_method :uri, :download_url

      def self.create(release)
        d = release.merge(release[:info_json])
        d.merge(download_url: Mod.extend_uri(d[:download_url]),
                released_at: DateTime.parse(d[:released_at]),
                dependencies: d[:dependencies].map(&Mod.method(:new)))
         .values_at(*Download.members)
         .then { Download.new(*_1) }
      end
    end
  end
end
